import angular from 'angular';
import 'klondike/klondike.js';
import 'ngDraggable';
import 'klondike/game.css!';


// Define the app
angular.module('solitaire', ['klondike', 'ngDraggable']);

// Then bootstrap angular
angular.element(document).ready(() => {
  angular.bootstrap(document, ['solitaire']);
});
